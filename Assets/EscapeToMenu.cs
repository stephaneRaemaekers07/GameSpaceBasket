﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeToMenu : MonoBehaviour {

    public string _sceneName="Main";

	void Update () {
        if(Input.GetKeyDown(KeyCode.Escape)){

            SceneManager.LoadScene(_sceneName);
        }
	}
}
